_flavor=postmarketos-allwinner
_config="config-${_flavor}.${CARCH}"

pkgname=linux-${_flavor}

pkgver=5.3.0_git20190816
pkgrel=1

arch="aarch64"
pkgdesc="Kernel fork with PinePhone Don't be evil patches"
url="https://gitlab.com/pine64-org/linux/"
makedepends="devicepkg-dev perl sed installkernel bash gmp-dev bc linux-headers elfutils-dev openssl-dev file bison flex rsync"
options="!strip !check !tracedeps"
_commit="299a91cfe91e310a178f9af5f73f9e4a6c61d46b"
_commit_rtl8723cs="1c9c0cb9d335626a66d8063399c6e16751ecc1a8"
source="
	${pkgname}-${_commit}.tar.gz::https://gitlab.com/pine64-org/linux/-/archive/${_commit}/linux-${_commit}.tar.gz
	rtl8723cs-${_commit_rtl8723cs}.tar.gz::https://github.com/Icenowy/rtl8723cs/archive/${_commit_rtl8723cs}.tar.gz
	config-${_flavor}.aarch64
	touch-dts.patch
	rtl8723cs.patch
"

subpackages="$pkgname-dev"

license="GPL2"
_abi_release=$_pkgver
_carch=${CARCH}
case "$_carch" in
aarch64*) _carch="arm64" ;;
arm*) _carch="arm" ;;
ppc*) _carch="powerpc" ;;
s390*) _carch="s390" ;;
esac

HOSTCC="${CC:-gcc}"
HOSTCC="${HOSTCC#${CROSS_COMPILE}}"

builddir="$srcdir/linux-${_commit}"

prepare() {
	default_prepare

	cp -rv "$srcdir"/rtl8723cs-${_commit_rtl8723cs} "$builddir"/drivers/staging/rtl8723cs

	REPLACE_GCCH=0 \
		downstreamkernel_prepare "$srcdir" "$builddir" "$_config" "$_carch" "$HOSTCC"
}

build() {
	unset LDFLAGS
	make ARCH="$_carch" CC="${CC:-gcc}" \
		KBUILD_BUILD_VERSION="$((pkgrel + 1 ))-${_flavor}" \
		CFLAGS_MODULE=-fno-pic
}

package() {
	# kernel.release
	install -D "$builddir/include/config/kernel.release" \
		"$pkgdir/usr/share/kernel/$_flavor/kernel.release"

	# zImage (find the right one)
	cd "$builddir/arch/$_carch/boot"
	_target="$pkgdir/boot/vmlinuz-$_flavor"
	for _zimg in zImage-dtb Image.gz-dtb *zImage Image; do
		[ -e "$_zimg" ] || continue
		msg "zImage found: $_zimg"
		install -Dm644 "$_zimg" "$_target"
		break
	done
	if ! [ -e "$_target" ]; then
		error "Could not find zImage in $PWD!"
		return 1
	fi

	cd "$builddir"
	local _install
	case "$CARCH" in
	aarch64*|arm*)	_install="modules_install dtbs_install" ;;
	*)		_install="modules_install" ;;
	esac

	make -j1 $_install \
		ARCH="$_carch" \
		INSTALL_MOD_PATH="$pkgdir" \
		INSTALL_DTBS_PATH="$pkgdir/usr/share/dtb"
}

dev() {
	provides="linux-headers"
	replaces="linux-headers"

	cd $builddir

	# https://github.com/torvalds/linux/blob/master/Documentation/kbuild/headers_install.rst
	make -j1 headers_install \
		ARCH="$_carch" \
		INSTALL_HDR_PATH="$subpkgdir"/usr
}

sha512sums="63d8b65f21a6d1c90ba5922bbdb7a4178eca7ea8bb814ce86c2af06e7c7fb8406d78be1c9609d8303b503d4de830a3f2cabd46658c4290f6b2f33dfdfd8bbc5b  linux-postmarketos-allwinner-299a91cfe91e310a178f9af5f73f9e4a6c61d46b.tar.gz
e4e585ce787301eeee07ff8e45a97343456808a47ed237053f0c83a49b4958a75514def4c5263f4507a282ea90bd2bb3f8f468839f7e5fd05ae947a796c8c6b9  rtl8723cs-1c9c0cb9d335626a66d8063399c6e16751ecc1a8.tar.gz
4ccd2e9d49fe2e246ca1b9c35258177c0df0df1599d4108d86a4b8e82ad9eee5395232df62f2e65688c51091d93ee85a1f363e0172d4a16f8a8be74d54e32d1a  config-postmarketos-allwinner.aarch64
5f2285e4eb262a9429c73599871c3fecbb858a388774bc7cba7631987ed1dc4a14cd7606e8fefc655c5128851fc853132f0a081e3d710c9266739af92f4d37e8  touch-dts.patch
bf06f931fb543f4bf2f0567902c3021de237fc9684f92af2ed3e956f869d907c9cff1cf3e11a72eb97bda47c8f8b28aff226634f641bdffa04b08d434f419faa  rtl8723cs.patch"
